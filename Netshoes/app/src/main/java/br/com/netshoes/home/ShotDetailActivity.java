package br.com.netshoes.home;

/*
 * Na definição do projeto existia a url de consulta ao detalhe do Shot. Porém,
 * o objeto que retorna é idêntico ao que já temos na tela anterior. Sendo assim,
 * interpretei que o esperado era utilizar a consulta web para obter dados mais
 * atualizados. Esta activity recebe mesmo assim o objeto Shot da tela anterior
 * para que ele possa servir de cache caso falhe a conexão com a internet.
 *
 * Ao invés de utilizar compartilhamento com api's de redes sociais utilizei o
 * Share Provider do Android por tornar o aplicativo mais abrangente. É possível
 * compartilhar o Shot com todas as redes sociais, enviar por email, bluetooth,
 * colocar nos aplicativos mais conhecidos (Keep, Evernote, Fancy, Pocket) ou até
 * mesmo abrir no browser para salvar no favoritos. Fica mais abrangente e mais
 * útil.
 *
 * Utilizei linkify na descrição do Shot, pois muitos usuários colocar url's para
 * suas páginas. Ao clicar nestes links abre o navegador para o usuário.
 *
 */

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.makeramen.RoundedImageView;

import br.com.netshoes.R;
import br.com.netshoes.dribbleservice.DribbleService;
import br.com.netshoes.entities.Shot;

public class ShotDetailActivity extends ActionBarActivity {


    private TextView labelShotTitle;
    private TextView labelShotViews;
    private TextView labelUserName;
    private TextView labelShotDescription;
    private ImageView imageViewShot;
    private RoundedImageView imageViewUser;
    private Shot shot;
    private ProgressBar progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.detalhe_shot);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        labelShotTitle = (TextView) findViewById(R.id.label_shot_title);
        labelShotViews = (TextView) findViewById(R.id.label_shot_views);
        labelUserName = (TextView) findViewById(R.id.label_username);
        labelShotDescription = (TextView) findViewById(R.id.label_shot_description);
        imageViewShot = (ImageView) findViewById(R.id.image_shot);
        imageViewUser = (RoundedImageView) findViewById(R.id.image_user);
        progress = (ProgressBar)findViewById(R.id.progress);


        if (getIntent() != null) {
            shot = (Shot) getIntent().getSerializableExtra("shot");
            if (shot != null) {
                getShot(shot.getId());
            }
        }
    }

    private void getShot(int shotId) {
        DribbleService.getShotDetails(shotDetailsCallback, shotId, this);
    }

    public FutureCallback<Shot> shotDetailsCallback = new FutureCallback<Shot>() {
        @Override
        public void onCompleted(Exception e, Shot result) {
            progress.setVisibility(View.GONE);
            if (e == null) {
                if (shot != null) {
                    showShot(result);
                } else {
                    showShot(shot);
                }
            } else {
                showShot(shot);
            }
        }
    };

    private void showShot(Shot shot) {
        Ion.with(imageViewShot)
                .placeholder(R.drawable.img_placeholder)
                .load(shot.getImage_url());

        Ion.with(imageViewUser)
                .placeholder(R.drawable.img_placeholder)
                .load(shot.getPlayer().getAvatar_url());

        labelShotTitle.setText(shot.getTitle());
        labelShotViews.setText(Integer.toString(shot.getViews_count()));
        labelUserName.setText(shot.getPlayer().getName());
        if (shot.getDescription() != null) {
            labelShotDescription.setText(Html.fromHtml(shot.getDescription()).toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_shot_detail, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);

        ShareActionProvider shareActionProvider = new ShareActionProvider(this);
        MenuItemCompat.setActionProvider(item, shareActionProvider);

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shot.getUrl());
        sendIntent.setType("text/plain");

        shareActionProvider.setShareIntent(sendIntent);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: finish(); break;
        }

        return true;

    }
}
