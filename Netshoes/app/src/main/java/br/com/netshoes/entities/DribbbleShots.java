package br.com.netshoes.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jpos on 07/07/15.
 */
public class DribbbleShots {

    private int page;
    @SerializedName("per_page")
    private int perPage;
    private int pages;
    private int total;
    @SerializedName("shots")
    private List<Shot> listShots;

    public int getPage() {
        return page;
    }

    public int getPerPage() {
        return perPage;
    }

    public int getPages() {
        return pages;
    }

    public int getTotal() {
        return total;
    }

    public List<Shot> getListShots() {
        return listShots;
    }
}
