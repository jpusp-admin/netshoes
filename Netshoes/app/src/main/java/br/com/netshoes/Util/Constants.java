package br.com.netshoes.Util;

/**
 * Created by jpos on 06/07/15.
 */
public class Constants {

    public static final String DRIBBLE_API_HOME_URL = "http://api.dribbble.com/";
    public static final String DRIBBLE_API_SHOT_LIST_URL = "shots/popular?page=";
    public static final String DRIBBLE_API_SHOT_DETAIL_URL = "shots/";
}
