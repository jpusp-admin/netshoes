package br.com.netshoes.entities;

import java.io.Serializable;

/**
 * Created by jpos on 06/07/15.
 */
public class Player implements Serializable {

    private int id;
    private String name;
    private String avatar_url;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAvatar_url() {
        return avatar_url;
    }
}
