package br.com.netshoes.entities;

import java.io.Serializable;

/**
 * Created by jpos on 06/07/15.
 */
public class Shot implements Serializable {

    private int id;
    private String title;
    private String description;
    private int views_count;
    private String url;
    private String image_url;
    private String image_teaser_url;
    private String image_400_url;
    private Player player;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getViews_count() {
        return views_count;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getImage_teaser_url() {
        return image_teaser_url;
    }

    public String getImage_400_url() {
        return image_400_url;
    }

    public Player getPlayer() {
        return player;
    }

    public String getUrl() {
        return url;
    }
}
