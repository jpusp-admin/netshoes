package br.com.netshoes.home;

/*
 * Observações importantes:
 *
 * Escolhi fazer a paginação do projeto no modo scroll infinito, ou seja,
 * quando o usuário chega no último item da lista é disparada uma consulta
 * web para trazer novos dados. Gmail, PlayStore, Youtube, AirBnb entre outros
 * funcionam assim.
 *
 * Quando não tem conexão com a internet, esta activity exibe um texto
 * informativo e um botão "tentar novamente". É uma ótima maneira de permitir
 * que o usuário verifique sua conexão e recarregue os dados sem sair do app.
 *
 * O carregamento das imagens é feito de forma assíncrona através da library
 * Ion, que também faz o cache em disco e cache em memória das imagens.
 *
 * Ao rotacionar o device não perde os dados nem existe reload. O layout
 * também é bem responsivo
 *
 * Existe duas opções para a exibição das imagens: exibir a imagem full ou
 * exibir uma imagem reduzida na lista inicial do app. A url destas imagens
 * é obtida na api do Dribbble. Optei por exibir a imagem full porque além
 * de ter uma qualidade melhor, é somente nesta url que obtemos os gifs.
 * As imagens em movimento na lista da home deixaram o app com uma aparência
 * viva e a qualidade da imagem é pixel perfect.
 *
 * O app usa os mais novos recursos Android: Toolbar, RecyclerView, AppCompat,
 * etc.
 *
 * Foi utilizado selectors para dar feedback ao toque no item da lista.
 *
 * O layout não tem overdraw de gpu. Apenas o texto sobre a imagem que fica
 * vermelho na depuração.
 *
 * Optei por ser conservador e não fugir do Mockup nem aumentar o número de libs.
 * Mas se eu pudesse fugir, faria o seguinte:
 *
 *  - faria uso do CardView.
 *  - iria retirar o texto que está por cima da imagem e colocaria no card, abaixo
 *    da imagem
 *  - a cor do card seria definida pela imagem do shot, utilizando a Palette Helper
 *    do Android
 *
 */

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.List;

import br.com.netshoes.R;
import br.com.netshoes.dribbleservice.DribbleService;
import br.com.netshoes.entities.DribbbleShots;
import br.com.netshoes.entities.Shot;


public class DribbleListActivity extends ActionBarActivity implements OnMoreListener {

    private SuperRecyclerView recyclerView;
    private int page = 1;
    private DribbbleShots dribbbleShots;
    private ShotsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dribble_list);

        recyclerView = (SuperRecyclerView) findViewById(R.id.recyclerview_shots);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setupMoreListener(this, 1);

        View emptyView = recyclerView.getEmptyView();
        if (emptyView != null) {
            Button buttonTryAgain = (Button) emptyView.findViewById(R.id.button_try_again);
            buttonTryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerView.showProgress();
                    recyclerView.getEmptyView().setVisibility(View.INVISIBLE);
                    DribbleService.getDribbbleShots(listShotsCallback, page, DribbleListActivity.this);
                }
            });
        }

        DribbleService.getDribbbleShots(listShotsCallback, 1, this);
    }

    public FutureCallback<DribbbleShots> listShotsCallback = new FutureCallback<DribbbleShots>() {
        @Override
        public void onCompleted(Exception e, DribbbleShots result) {
            if (e == null) {
                if (adapter == null) {
                    dribbbleShots = result;
                    adapter = new ShotsListAdapter();
                    recyclerView.setAdapter(adapter);
                } else {
                    adapter.addAll(result.getListShots());
                }

                recyclerView.showRecycler();
            } else {
                recyclerView.hideRecycler();
                recyclerView.hideProgress();
                recyclerView.getEmptyView().setVisibility(View.VISIBLE);
            }

            recyclerView.hideMoreProgress();
        }
    };

    @Override
    public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {
        if (page < dribbbleShots.getPages()) {
            page++;
            DribbleService.getDribbbleShots(listShotsCallback, page, this);
        }
    }


    //-------------------------------------------------------------------------------------------------

    public class ShotsListAdapter extends RecyclerView.Adapter<DribbleListActivity.ViewHolder> {

       @Override
        public DribbleListActivity.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shot_list_item, parent, false);
            view.findViewById(R.id.root_view).setOnClickListener(itemClickListener);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(DribbleListActivity.ViewHolder holder, int position) {
            Shot shot = dribbbleShots.getListShots().get(position);

            holder.labelShotTitle.setText(shot.getTitle());
            holder.labelShotViews.setText(Integer.toString(shot.getViews_count()));
            holder.rootView.setTag(shot);
            Ion.with(holder.imageViewShot)
                    .placeholder(R.drawable.img_placeholder)
                    .load(shot.getImage_url());
        }

        @Override
        public int getItemCount() {
            return dribbbleShots != null ? dribbbleShots.getListShots().size() : 0;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void addAll(List<Shot> newShotsList) {
            int startIndex = dribbbleShots.getListShots().size();
            dribbbleShots.getListShots().addAll(startIndex, newShotsList);
            notifyItemRangeInserted(startIndex, newShotsList.size());
        }

        public View.OnClickListener itemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DribbleListActivity.this, ShotDetailActivity.class);
                intent.putExtra("shot", (Shot) v.getTag());
                startActivity(intent);
            }
        };
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView labelShotTitle;
        public TextView labelShotViews;
        public ImageView imageViewShot;
        public View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            labelShotTitle = (TextView) itemView.findViewById(R.id.label_shot_title);
            labelShotViews = (TextView) itemView.findViewById(R.id.label_shot_views);
            imageViewShot = (ImageView) itemView.findViewById(R.id.image_shot);
            rootView = itemView.findViewById(R.id.root_view);
        }
    }

}
