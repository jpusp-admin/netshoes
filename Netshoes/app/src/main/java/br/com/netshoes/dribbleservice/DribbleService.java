package br.com.netshoes.dribbleservice;

import android.content.Context;

import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import br.com.netshoes.Util.Constants;
import br.com.netshoes.entities.DribbbleShots;
import br.com.netshoes.entities.Shot;

/**
 * Created by jpos on 06/07/15.
 */
public class DribbleService {

    public static Future<DribbbleShots> getDribbbleShots(FutureCallback<DribbbleShots> futureCallback, int page, Context context) {
        return Ion.with(context)
                  .load(Constants.DRIBBLE_API_HOME_URL + Constants.DRIBBLE_API_SHOT_LIST_URL + page)
                  .setTimeout(15000)
                  .as(DribbbleShots.class)
                  .setCallback(futureCallback);
    }

    public static Future<Shot> getShotDetails(FutureCallback<Shot> futureCallback, int shotId, Context context) {
        return Ion.with(context)
                .load(Constants.DRIBBLE_API_HOME_URL + Constants.DRIBBLE_API_SHOT_DETAIL_URL + shotId)
                .setTimeout(15000)
                .as(Shot.class)
                .setCallback(futureCallback);
    }
}
